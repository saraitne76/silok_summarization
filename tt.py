import tensorflow as tf


a = tf.random_uniform([64, 100, 300])

b = tf.gather(a, [0,1])
c = tf.gather(a, 1)

print(b, c)