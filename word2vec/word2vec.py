from utils import *
from gensim.models import Word2Vec
import logging
import os


def word2vec(data_dir, model_dir, word_dim, window_size, word_min_freq, n_processes, n_iter):
    data = read_data(data_dir)
    print('number of data: %d' % len(data))

    model = Word2Vec(data,
                     size=word_dim,
                     window=window_size,
                     min_count=word_min_freq,
                     workers=n_processes,
                     sg=1,
                     iter=n_iter)
    os.makedirs(model_dir, exist_ok=True)
    model.save(os.path.join(model_dir, 'word2vec.embed'))


def main():
    # for window, min_word_cnt in [(8, 2), (8, 4), (16, 2), (16, 4)]:
    for window, min_word_cnt in [(8, 2), (8, 4)]:
        data_list = ['data/Hannanum', 'data/Kkma', 'data/Mecab', 'data/Okt']
        workers = 8
        learning_iter = 50
        dim = 300

        for data in data_list:
            destination = 'models/%s_d%03d_w%02d_mc%d' % (data.split('_')[-1], dim, window, min_word_cnt)
            os.makedirs(destination, exist_ok=True)
            # print(destination

            fomatter = logging.Formatter('%(asctime)s:  %(levelname)s : %(message)s')

            file_handler = logging.FileHandler(os.path.join(destination, 'log.txt'))
            file_handler.setFormatter(fomatter)
            stream_handler = logging.StreamHandler()
            stream_handler.setFormatter(fomatter)

            logging.basicConfig(level=logging.INFO, handlers=(file_handler, stream_handler))

            word2vec(data_dir=data,
                     model_dir=destination,
                     word_dim=dim,
                     window_size=window,
                     word_min_freq=min_word_cnt,
                     n_processes=workers,
                     n_iter=learning_iter)

            logging.root.removeHandler(file_handler)
            logging.root.removeHandler(stream_handler)
            file_handler.close()
            stream_handler.close()


if __name__ == '__main__':
    main()
