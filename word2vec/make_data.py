from utils import *
import re
from konlpy.tag import Kkma, Komoran, Hannanum, Okt, Mecab
from multiprocessing import Process, Queue
import random

PATTERN = '\(\w+\)|\d+\)|〔\w+〕'     # remove hanja & number of juseok


def get_txt_paths(basic_path, kings):
    txts = []
    for king in kings:
        king_path = os.path.join(basic_path, king)
        books = os.listdir(king_path)
        for book in books:
            book_path = os.path.join(king_path, book)
            texts = os.listdir(book_path)
            for text in texts:
                text_path = os.path.join(book_path, text)
                txts.append(text_path)
    return txts


def file2data(file_path: str, re_pattern, konlpy_tagger) -> List[str]:
    kr = get_kr(file_path).split('\n')
    kr = remove_redundant_line(kr, check=1)
    kr = ' '.join(kr)
    kr = re_pattern.sub('', kr)
    try:
        kr = konlpy_tagger.morphs(kr)
    except ValueError:
        kr = None

    title = get_title(file_path)
    title = re_pattern.sub('', title)
    try:
        title = konlpy_tagger.morphs(title)
    except ValueError:
        title = None

    out = [s for s in [kr, title] if s is not None]
    return out


def data_write(destination: str,
               pickle_name: str,
               files: List[str],
               q: Queue,
               tagger_class):
    pattern = re.compile(PATTERN)
    tagger = tagger_class()
    data_list = list()
    while q.qsize() > 0 or not q.empty():
        txt = files[q.get()]
        data_list.extend(file2data(txt, pattern, tagger))
        print('\rRemain Data %d' % (q.qsize()), end='')
        if q.qsize() < 1 or q.empty():
            break
    os.makedirs(destination, exist_ok=True)
    pickle_store(data_list, os.path.join(destination, pickle_name))
    print('\n%s store' % pickle_name)


def main(n_process, destination, kings, tagger_class):
    txt_files = get_txt_paths(SILOK_PATH, kings)
    n_files = len(txt_files)
    print('The number of text_files : %d' % n_files)

    q = Queue()
    for i in range(n_files):
        q.put(i)

    processes = []
    for i in range(n_process):
        p = Process(target=data_write,
                    args=(destination, 'data_%02d.p' % i, txt_files, q, tagger_class))
        p.start()
        processes.append(p)

    for p in processes:
        p.join()


if __name__ == '__main__':
    number_of_processes = 8

    target_folder = 'data_Okt'
    Tagger = Okt
    main(number_of_processes, target_folder, kings_total, Tagger)

    data = read_data(target_folder)
    print('The number of made data : %d' % len(data))
    print('example data')
    n_examples = 10
    for _ in range(n_examples):
        print(random.choice(data))

    # ============================================================================================

    # target_folder = 'data_Komoran'
    # Tagger = Komoran
    # main(number_of_processes, target_folder, kings_total, Tagger)
    #
    # data = read_data(target_folder)
    # print('The number of made data : %d' % len(data))
    # print('example data')
    # n_examples = 10
    # for _ in range(n_examples):
    #     print(random.choice(data))

    # ============================================================================================

    # target_folder = 'data_Hannanum'
    # Tagger = Hannanum
    # main(number_of_processes, target_folder, kings_total, Tagger)
    #
    # data = read_data(target_folder)
    # print('The number of made data : %d' % len(data))
    # print('example data')
    # n_examples = 10
    # for _ in range(n_examples):
    #     print(random.choice(data))
