"""
utils
"""
from typing import List, Optional, Dict, Union, Tuple
import pickle
import os
from copy import deepcopy
import numpy as np
from gensim.models import Word2Vec
import random
import tensorflow as tf


kings_train = ['k01_Taejo', 'k02_Jeongjong', 'k03_Taejong', 'k04_Sejong', 'k05_Munjong', 'k06_Danjong',
               'k07_Sejo', 'k08_Yejong', 'k09_Seongjong', 'k10_Yeonsangun', 'k11_Jungjong', 'k12_Injong',
               'k13_Myungjong', 'k14_Seonjo', 'k14_Seonjo_sujeong', 'k15_Gwanghaegun_jeongcho',
               'k15_Gwanghaegun_jungcho', 'k16_Injo', 'k17_Hyojong', 'k18_Hyeonjong', 'k18_Hyeonjong_gaesu',
               'k19_Sukjong', 'k19_Sukjong_bogwoljeongo', 'k20_Gyeongjong', 'k20_Gyeongjong_sujeong']

kings_test = ['k21_Yeongjo', 'k22_Jeongjo', 'k23_Sunjo', 'k24_Heonjong',
              'k25_Cheoljong', 'k26_Gojong', 'k27_Sunjong', 'k27_Sunjong_burok']


kings_total = sorted(kings_train + kings_test)


SILOK_PATH = '/home/miracle/CRC_DATA/chosun-utf8/'
ENC_BUCKET = [50, 100, 200, 300, 400, 500]
DOC_BUCKET = ENC_BUCKET
DEC_MAX_LENGTH = 40
TITLE_MAX_LENGTH = DEC_MAX_LENGTH - 1
PAD = 'PAD'
UNK = 'UNK'
GO = 'GO'
EOS = 'EOS'
SPECIAL_WORDS = [PAD, UNK, GO, EOS]


class Tokenized:
    def __init__(self,
                 file_name: str,
                 title_raw: str,
                 title_token: List[str],
                 doc_raw: str,
                 doc_token: List[str]):
        self.file_name = file_name
        self.title_raw = title_raw
        self.title_token = title_token
        self.doc_raw = doc_raw
        self.doc_token = doc_token

    def printing(self):
        print('file_name:   %s' % self.file_name)
        print('title_raw:   %s' % self.title_raw)
        print('title_token: %s' % self.title_token)
        print('doc_raw:     %s' % self.doc_raw)
        print('doc_token:   %s' % self.doc_token)
        return 0


class EncDecData(Tokenized):
    def __init__(self,
                 tokenized: Tokenized,
                 word2idx: Dict):
        super(EncDecData, self).__init__(file_name=tokenized.file_name,
                                         title_raw=tokenized.title_raw,
                                         title_token=tokenized.title_token,
                                         doc_raw=tokenized.doc_raw,
                                         doc_token=tokenized.doc_token)

        doc_padded = padding(self.doc_token, get_bucket_size(self.doc_token))
        self.enc_input = word_indexing(doc_padded, word2idx)

        title_padded = padding([GO] + self.title_token, DEC_MAX_LENGTH)
        self.dec_input = word_indexing(title_padded, word2idx)

        title_padded = padding(self.title_token + [EOS], DEC_MAX_LENGTH)
        self.dec_output = word_indexing(title_padded, word2idx)

        self.enc_seq_len, self.dec_seq_len = self._seq_len(word2idx)

        self.enc_full_len = len(self.enc_input)
        assert len(self.dec_input) == len(self.dec_output)
        self.dec_full_len = len(self.dec_output)

    def _seq_len(self, word2idx):
        enc_pad_idx = np.where(self.enc_input == word2idx[PAD])[0]
        if len(enc_pad_idx) == 0:
            enc_seq_len = len(self.doc_token)
        else:
            if enc_pad_idx[0] == len(self.doc_token):
                enc_seq_len = len(self.doc_token)
            else:
                self.printing()
                self.printing_full(dict_swap(word2idx))
                raise ValueError('Encoder sequence length error')

        dec_pad_idx = np.where(self.dec_input == word2idx[PAD])[0]
        if not np.all(dec_pad_idx == np.where(self.dec_output == word2idx[PAD])[0]):
            raise ValueError('Decoder indexed sentence error')
        if len(dec_pad_idx) == 0:
            dec_seq_len = len(self.title_token) + 1
        else:
            if dec_pad_idx[0] == len(self.title_token) + 1:
                dec_seq_len = len(self.title_token) + 1
            else:
                raise ValueError('Decoder sequence length error')

        return enc_seq_len, dec_seq_len

    def printing_full(self, idx2word: Dict):
        print('file_name:   %s' % self.file_name)
        print('doc_raw:     %s' % self.doc_raw)
        print('doc_token:   %s' % self.doc_token)
        print('enc_input:   %s' % self.enc_input)
        print('enc_input:   %s' % indexed_word_revert(self.enc_input, idx2word))
        print('enc_seq_len: %d' % self.enc_seq_len)
        print('enc_full_len:%d' % self.enc_full_len)
        print('title_raw:   %s' % self.title_raw)
        print('title_token: %s' % self.title_token)
        print('dec_input:   %s' % self.dec_input)
        print('dec_input:   %s' % indexed_word_revert(self.dec_input, idx2word))
        print('dec_output:  %s' % self.dec_output)
        print('dec_output:  %s' % indexed_word_revert(self.dec_output, idx2word))
        print('dec_seq_len: %d' % self.dec_seq_len)
        print('dec_full_len:%d' % self.dec_full_len)
        return 0


class EncDecBatch:
    def __init__(self, samples: List[EncDecData]):
        enc_full_len = np.unique([sample.enc_full_len for sample in samples])
        if len(enc_full_len) != 1:
            raise ValueError('Padded length is not equal')
        else:
            self.enc_full_len = int(enc_full_len[0])

        dec_full_len = np.unique([sample.dec_full_len for sample in samples])
        if len(dec_full_len) != 1:
            raise ValueError('Padded length is not equal')
        else:
            self.dec_full_len = int(dec_full_len[0])

        self.size = len(samples)
        self.enc_input = np.zeros((self.size, self.enc_full_len), np.uint32)
        self.dec_input = np.zeros((self.size, self.dec_full_len), np.uint32)
        self.dec_output = np.zeros((self.size, self.dec_full_len), np.uint32)
        self.enc_seq_len = np.zeros(self.size, np.uint16)
        self.dec_seq_len = np.zeros(self.size, np.uint16)
        for i, sample in enumerate(samples):
            self.enc_input[i, :] = sample.enc_input
            self.dec_input[i, :] = sample.dec_input
            self.dec_output[i, :] = sample.dec_output
            self.enc_seq_len[i] = sample.enc_seq_len
            self.dec_seq_len[i] = sample.dec_seq_len

    def printing(self, idx2word: Dict):
        for i in range(self.size):
            print('==============================================================================================')
            print('enc_input:   %s' % self.enc_input[i])
            print('enc_input:   %s' % indexed_word_revert(self.enc_input[i], idx2word))
            print('enc_seq_len: %d' % self.enc_seq_len[i])
            print('enc_full_len:%d' % self.enc_full_len)
            print('dec_input:   %s' % self.dec_input[i])
            print('dec_input:   %s' % indexed_word_revert(self.dec_input[i], idx2word))
            print('dec_output:  %s' % self.dec_output[i])
            print('dec_output:  %s' % indexed_word_revert(self.dec_output[i], idx2word))
            print('dec_seq_len: %d' % self.dec_seq_len[i])
            print('==============================================================================================')


def print_write(s, file, mode='a'):
    if isinstance(file, str):
        f = open(file, mode)
        print(s, end='')
        f.write(s)
        f.close()
    else:
        print(s, end='')
        file.write(s)


def rnn_cell(cell_type: str, hidden: int, keep_prob, output_drop=True, state_drop=True):
    if cell_type == 'LSTM':
        cell = tf.nn.rnn_cell.LSTMCell(hidden, state_is_tuple=True)
    elif cell_type == 'GRU':
        cell = tf.nn.rnn_cell.GRUCell(hidden)
    elif cell_type == 'RNN':
        cell = tf.nn.rnn_cell.RNNCell(hidden)
    else:
        raise TypeError('cell name error')

    if state_drop and output_drop:
        cell = tf.nn.rnn_cell.DropoutWrapper(cell,
                                             output_keep_prob=keep_prob,
                                             state_keep_prob=keep_prob,
                                             variational_recurrent=True,
                                             dtype=tf.float32)
    elif output_drop:
        cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=keep_prob)
    elif state_drop:
        cell = tf.nn.rnn_cell.DropoutWrapper(cell,
                                             state_keep_prob=keep_prob,
                                             variational_recurrent=True,
                                             dtype=tf.float32)
    else:
        pass
    return cell


def rnn_cells(cell_type: str, hidden: int, layer: int, keep_prob, output_drop=True, state_drop=True):
    multi_cell = []

    for _ in range(layer):
        cell = rnn_cell(cell_type, hidden, keep_prob, output_drop, state_drop)

        multi_cell.append(cell)
    return multi_cell


def get_tf_config():
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return config


def bucketting(data: List[EncDecData]):
    bucketted = [list() for _ in range(len(ENC_BUCKET))]

    for d in data:
        k = ENC_BUCKET.index(d.enc_full_len)
        bucketted[k].append(d)

    n_data = np.array([len(a) for a in bucketted]).sum()
    bucket_prob = np.zeros(len(ENC_BUCKET))
    for i in range(len(ENC_BUCKET)):
        bucket_prob[i] = len(bucketted[i]) / n_data
    if bucket_prob.sum() < 0.9999:
        raise ValueError('bucket_prob sum is not 1.0 -> sum=%f' % bucket_prob.sum())
    return bucketted, bucket_prob


def get_batch(bucketted: List[List[EncDecData]], bucket_prob: np.ndarray, batch_size: int) -> EncDecBatch:
    """
    Get batch data using bucketted LineData
    :param bucketted: bucketted LineData (number of bucket, number of data in the bucket)
    :param bucket_prob: The probability that the bucket will be selected.
                        according to the number of data in the bucket.
                        the number of data in the bucket is increase, the probability the bucket selected is increase.
    :param batch_size: the number of data in the batch
    :return: BatchData
    """
    t = np.random.choice(len(bucketted), 1, False, bucket_prob)[0]
    try:
        samples = random.sample(bucketted[t], batch_size)
    except ValueError:
        samples = random.sample(bucketted[t], len(bucketted[t]))
    return EncDecBatch(samples)


def uni2dec(c):
    encoded = list(c.encode('unicode-escape'))[2:]
    if not encoded:
        return 0
    h = ''
    for c in encoded:
        h += chr(c)
    return int(h, 16)


def unicode_classify(c):
    dec = uni2dec(c)
    if (int('4E00', 16) <= dec <= int('9FFF', 16) or
            int('3400', 16) <= dec <= int('4DBF', 16) or
            int('F900', 16) <= dec <= int('FAFF', 16)):
        return 0    # Chinese
    elif int('AC00', 16) <= dec <= int('D7AF', 16):
        return 1    # Korean
    else:
        return 2    # else


def dict_swap(my_dic: Dict):
    return {v: k for k, v in my_dic.items()}


def pickle_store(obj, path):
    f = open(path, 'wb')
    pickle.dump(obj, f)
    f.close()
    return


def pickle_load(path):
    f = open(path, 'rb')
    a = pickle.load(f)
    f.close()
    return a


def text_readlines(file_path):
    f = open(file_path, 'r', encoding='utf-8')
    t = f.readlines()
    f.close()
    return t


def text_read(file_path):
    f = open(file_path, 'r', encoding='utf-8')
    t = f.read()
    f.close()
    return t


def read_data(folder):
    files = os.listdir(folder)
    d = []
    for f in files:
        if f.split('.')[-1] == 'p':
            d = d + pickle_load(os.path.join(folder, f))
    return d


def get_kr(text_file: str):
    t = text_read(text_file)
    juseok_idx = t.find('[註')
    if juseok_idx == -1:
        t = t[t.find('국역\n')+3:t.find('원문\n')]
    else:
        t = t[t.find('국역\n')+3:min(t.find('원문\n'), juseok_idx)]
    idx = 0
    for i, c in enumerate(t):
        if unicode_classify(c) == 1:
            idx = i
            break
    return t[idx:]


def remove_redundant_line(lines: List[str], check=1) -> List[str]:
    new_lines = []
    for line in lines:
        f = False
        for c in line:
            if unicode_classify(c) == check:    # 0 hanja, 1 hangeul
                f = True
                break
        if f:
            new_lines.append(line)
    return new_lines


def get_title(text_file: str) -> str:
    t = remove_redundant_line(text_readlines(text_file), 1)
    if t[2].find('국역') < 0:
        raise ValueError('finding title fail')
    else:
        return t[1]


def padding(tokenized: List[str], max_len: int) -> List[str]:
    pad = [PAD for _ in range(max_len - len(tokenized))]
    padded = deepcopy(tokenized)
    padded.extend(pad)
    return padded


def get_bucket_size(tokenized: List[str]) -> Optional[int]:
    s_len = len(tokenized)
    for b_len in DOC_BUCKET:
        if b_len >= s_len:
            return b_len
    return None


def word_indexing(tokenized: List[str], word2idx: Dict) -> np.ndarray:
    indexed = np.zeros(len(tokenized), np.uint32)
    for i in range(len(tokenized)):
        try:
            indexed[i] = word2idx[tokenized[i]]
        except KeyError:
            indexed[i] = word2idx[UNK]
    return indexed


def indexed_word_revert(indexed: np.ndarray, idx2word: Dict, upto_eos=False) -> List[str]:
    reverted = list()
    if upto_eos:
        for idx in indexed:
            w = idx2word[idx]
            if w == EOS:
                break
            else:
                reverted.append(w)
    else:
        for idx in indexed:
            reverted.append(idx2word[idx])
    return reverted


def load_word2vec(model: Union[Word2Vec, str],
                  init_from_wv=True) -> Tuple[Dict, np.ndarray, int]:
    """
    :param model: gensim word2vec model
    :param init_from_wv: initiate Unknown, Start, End word with normal~(mean(wv), std(wv))
    :return: word dictionary, embedding vectors, embedding size
    """
    """
    :param model: gensim word2vec model
    :return: word dictionary, embedding vectors, embedding size
    """
    if isinstance(model, str):
        model = Word2Vec.load(model)

    size = model.wv.vectors.shape[1]

    pad = np.zeros((1, size))
    if init_from_wv:
        m = np.mean(model.wv.vectors)
        s = np.std(model.wv.vectors)
        unk = np.random.normal(size=(1, size), loc=m, scale=s)
        go = np.random.normal(size=(1, size), loc=m, scale=s)
        eos = np.random.normal(size=(1, size), loc=m, scale=s)
    else:
        unk = np.random.normal(size=(1, size))
        go = np.random.normal(size=(1, size))
        eos = np.random.normal(size=(1, size))
    init = np.concatenate((pad, unk, go, eos), axis=0)

    assert init.shape[0] == len(SPECIAL_WORDS)

    word2idx = {c: i for i, c in enumerate(SPECIAL_WORDS + model.wv.index2word)}
    vectors = np.concatenate((init, model.wv.vectors), axis=0)
    return word2idx, vectors, size


def inverse_tagging(tagged: List[Tuple[str, str]]) -> str:
    out = ''
    for word, tag in tagged:
        out += word
        if tag.find('J') > -1:
            out += ' '
    return out
