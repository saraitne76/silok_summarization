from models.doc_embedding import *

os.environ['CUDA_VISIBLE_DEVICES'] = '1'

model = 'train_model/doc_embedding_P5e-3_lstm2_256_kkma300_8_2'
embed_model = 'word2vec/models/Kkma_d300_w08_mc2/word2vec.embed'

word2idx, vectors, embed_size = load_word2vec(embed_model)
train_data = pickle_load('data/EncDecData/Kkma/train.p')

summmarizer = Summarizer(logdir=model,
                         cell_type='LSTM',
                         word2idx=word2idx,
                         embedding_size=embed_size,
                         num_hidden=256,
                         num_layers=2,
                         embedding_vectors=vectors,
                         forward_only=False,
                         batch_size=64,
                         keep_prob=0.75,
                         beam_width=None,
                         p_coef=5e-3)
sess = tf.Session(config=get_tf_config())

summmarizer.train(sess=sess,
                  train_step=50000,
                  lr=1e-3,
                  train_data=train_data,
                  ckpt=None)

summmarizer.train(sess=sess,
                  train_step=100000,
                  lr=1e-4,
                  train_data=train_data,
                  ckpt='weights-50000')
sess.close()
