from models.bahdanau_attention import *
from konlpy.tag import Kkma

doc_string = '영의정 김상철이 아뢰기를, "청정하신 뒤에는 사체가 절로 다른데, 대조 의 연중에 나는 감히 참여하여 들을 수 없다고 하신다면, 나라의 일이 장차 어떻게 되겠습니까?" 하였다.'

os.environ['CUDA_VISIBLE_DEVICES'] = '1'

model = 'train_model/bahdanau_lstm2_256_kkma300_8_2'
embed_model = 'word2vec/models/Kkma_d300_w08_mc2/word2vec.embed'

word2idx, _, embed_size = load_word2vec(embed_model)

summmarizer = Summarizer(logdir=model,
                         cell_type='LSTM',
                         word2idx=word2idx,
                         embedding_size=embed_size,
                         num_hidden=256,
                         num_layers=2,
                         forward_only=True,
                         embedding_vectors=None,
                         beam_width=10,
                         batch_size=1,
                         keep_prob=1.0)

sess = tf.Session(config=get_tf_config())
kkma = Kkma()
summmarizer.init_model(sess, ckpt='weights-90000')
summ = summmarizer.run(sess, doc_string, kkma)
print(summ)

sess.close()
