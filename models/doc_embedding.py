import tensorflow as tf
from tensorflow.contrib import rnn, seq2seq
from utils import *
import time
from rouge import *
from tensorflow.python.ops import control_flow_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.util import nest
from tensorflow.python.framework import ops
from tensorflow.contrib.seq2seq.python.ops.beam_search_decoder import _beam_search_step


class Summarizer(object):
    def __init__(self,
                 logdir: str,
                 cell_type: str,
                 word2idx: Dict,
                 embedding_size: int,
                 num_hidden: int,
                 num_layers: int,
                 forward_only: bool,
                 batch_size: int,
                 keep_prob: float,
                 p_coef: float,
                 beam_width: Optional[int],
                 embedding_vectors=None,
                 d_a=None,
                 output_drop=True,
                 state_drop=True,
                 train_log='Train_log.txt',
                 test_log='Test_log.txt'):

        self.logit = None
        self.prediction = None
        self.loss = None
        self.optimizer = None

        self.logdir = logdir
        os.makedirs(self.logdir, exist_ok=True)
        self.train_log = train_log
        self.test_log = test_log

        self.cell_type = cell_type
        self.word2idx = word2idx
        self.idx2word = dict_swap(word2idx)
        self.vocabulary_size = len(word2idx)
        self.embedding_size = embedding_size
        self.num_hidden = num_hidden
        self.num_layers = num_layers
        self.beam_width = beam_width
        self.batch_size = batch_size
        self.keep_prob = keep_prob
        self.output_drop = output_drop
        self.state_drop = state_drop

        self.encoder_input = tf.placeholder(tf.int32, [None, None], name='encoder_input')
        self.encoder_seq_len = tf.placeholder(tf.int32, [None], name='encoder_seq_len')
        self.decoder_input = tf.placeholder(tf.int32, [None, None], name='decoder_input')
        self.decoder_seq_len = tf.placeholder(tf.int32, [None], name='decoder_seq_len')
        self.decoder_target = tf.placeholder(tf.int32, [None, None], name='decoder_target')
        self.global_step = tf.get_variable('global_step', [],
                                           dtype=tf.int32,
                                           initializer=tf.initializers.constant(0),
                                           trainable=False)

        self.lr = tf.get_variable('learning_rate', [],
                                  dtype=tf.float32,
                                  initializer=tf.initializers.zeros(),
                                  trainable=False)
        self.lr_ = tf.placeholder(tf.float32)
        self.lr_update = tf.assign(self.lr, self.lr_)

        with tf.variable_scope("embedding"):
            self.embeddings = self._embeddings(embedding_vectors)
            self.encoder_emb_inp = tf.nn.embedding_lookup(self.embeddings, self.encoder_input)
            self.decoder_emb_inp = tf.nn.embedding_lookup(self.embeddings, self.decoder_input)

        with tf.variable_scope("encoder"):
            self.encoder_output, self.encoder_state = self._encoder()

        with tf.variable_scope("attention"):
            if d_a is None:
                d_a = 2*self.num_hidden
            self.M, self.A = self._attention(d_a, DEC_MAX_LENGTH)

        with tf.variable_scope("decoder") as scope:
            self.decoder_cell = rnn_cell(self.cell_type,
                                         self.num_hidden*2,
                                         self.keep_prob,
                                         self.output_drop,
                                         self.state_drop)
            self.projection_layer = tf.layers.Dense(self.vocabulary_size, use_bias=False, name='projection')

            if forward_only:
                self.prediction = self._decoder_generator(scope)
            else:
                self.logit = self._decoder_trainer(scope)

        if not forward_only:
            self.loss = self._get_loss(p_coef)
            self.optimizer = self._get_optimizer()

            self.loss_, self.loss_op = tf.metrics.mean(self.loss)
            tf.summary.scalar('loss', self.loss_)
            tf.summary.scalar('learning rate', self.lr)
        else:
            self.rouge_1 = tf.placeholder(tf.float32)
            self.rouge_2 = tf.placeholder(tf.float32)
            self.rouge_lcs = tf.placeholder(tf.float32)
            tf.summary.scalar('rouge_1', self.rouge_1)
            tf.summary.scalar('rouge_2', self.rouge_2)
            tf.summary.scalar('rouge_lcs', self.rouge_lcs)

        self.merged = tf.summary.merge_all()
        self.saver = tf.train.Saver()
        self.get_trainable_var = tf.trainable_variables()
        self.global_var_init = tf.global_variables_initializer()
        self.local_var_init = tf.local_variables_initializer()

    def init_model(self, sess, log_file=None, ckpt=None):
        if ckpt:
            self.saver.restore(sess, os.path.join(self.logdir, ckpt))
            if log_file:
                print_write('model loaded from file: %s\n' % os.path.join(self.logdir, ckpt),
                            os.path.join(self.logdir, log_file), 'a')
        else:
            f = open(os.path.join(self.logdir, log_file), 'w')
            sess.run(self.global_var_init)
            print_write('global variable initialize\n', f)
            writer = tf.summary.FileWriter(os.path.join(self.logdir, 'train'), filename_suffix='-graph')
            writer.add_graph(sess.graph)

            print_write('============================================================\n', f)
            count_vars = 0
            for var in self.get_trainable_var:
                name = var.name
                shape = var.shape.as_list()
                num_elements = var.shape.num_elements()
                print_write('Variable name: %s\n' % name, f)
                print_write('Placed device: %s\n' % var.device, f)
                print_write('Shape : %s  Elements: %d\n' % (str(shape), num_elements), f)
                print_write('============================================================\n', f)
                count_vars = count_vars + num_elements
            print_write('Total number of trainable variables %d\n' % count_vars, f)
            print_write('============================================================\n', f)
            f.close()
            writer.close()
        return

    def train(self,
              sess: tf.Session,
              train_step: int,
              lr: float,
              train_data: List[EncDecData],
              ckpt=None,
              summary_step=1000):
        assert (train_step % summary_step) == 0
        self.init_model(sess, self.train_log, ckpt)
        bucketted, bucket_prob = bucketting(train_data)

        global_step = sess.run(self.global_step)
        base_step = global_step
        sess.run([self.local_var_init, self.lr_update], feed_dict={self.lr_: lr})
        print_write('Step %d,  learning rate: %f,  time: %s\n'
                    % (global_step, sess.run(self.lr), time.strftime('%y-%m-%d %H:%M:%S')),
                    os.path.join(self.logdir, self.train_log), 'a')
        print_write('Number of data: %d\n' % len(train_data), os.path.join(self.logdir, self.train_log), 'a')

        s = time.time()
        for i in range(train_step//(summary_step*10)):
            writer = tf.summary.FileWriter(os.path.join(self.logdir, 'train'),
                                           filename_suffix='-step-%d' % global_step)
            for j in range(summary_step*10):
                batch = get_batch(bucketted, bucket_prob, self.batch_size)

                feed = {self.encoder_input: batch.enc_input,
                        self.encoder_seq_len: batch.enc_seq_len,
                        self.decoder_input: batch.dec_input,
                        self.decoder_target: batch.dec_output,
                        self.decoder_seq_len: batch.dec_seq_len}
                fetch = [self.optimizer, self.loss_op]
                _, loss = sess.run(fetch, feed_dict=feed)
                global_step = sess.run(self.global_step)

                print('\rTraining - Loss: %0.3f, step %d/%d'
                      % (loss, global_step, train_step + base_step), end='')

                if global_step % summary_step == 0:
                    fetch = [self.merged, self.loss_]
                    merged, loss = sess.run(fetch)
                    writer.add_summary(merged, global_step)

                    print('\r', end='')
                    print_write('Training - Loss: %0.3f, step: %d, %0.2f sec/step\n'
                                % (loss, global_step, (time.time() - s) / summary_step),
                                os.path.join(self.logdir, self.train_log), 'a')

                    s = time.time()
                    sess.run(self.local_var_init)

            print_write('global step: %d, model save, time: %s\n'
                        % (global_step, time.strftime('%y-%m-%d %H:%M:%S')),
                        os.path.join(self.logdir, self.train_log), 'a')
            self.saver.save(sess, os.path.join(self.logdir, 'weights'), global_step)
            writer.close()
            s = time.time()
        return

    def eval(self,
             sess: tf.Session,
             test_data: List[EncDecData],
             ckpt=None):
        s = time.time()
        cnt = 0
        n_data = len(test_data)
        self.init_model(sess, self.test_log, ckpt)
        global_step = sess.run(self.global_step)
        bucketted, bucket_prob = bucketting(test_data)

        print_write('Number of data: %d\n' % len(test_data), os.path.join(self.logdir, self.test_log), 'a')
        rouge_1_ = []
        rouge_2_ = []
        rouge_lcs_ = []

        for bucket in bucketted:
            len_bucket = len(bucket)
            idx = 0
            escape = False
            while True:
                samples = list()
                while True:
                    if bucket[idx].enc_seq_len != 0 and bucket[idx].dec_seq_len != 0:
                        samples.append(bucket[idx])
                    idx += 1
                    if idx >= len_bucket:
                        escape = True
                        break
                    if len(samples) == self.batch_size:
                        break
                if len(samples) != self.batch_size:
                    break

                batch = EncDecBatch(samples)

                feed = {self.encoder_input: batch.enc_input,
                        self.encoder_seq_len: batch.enc_seq_len}
                prediction = sess.run(self.prediction, feed_dict=feed)
                prediction = prediction[:, :, 0]      # for beamsearch decoding

                rouge_1 = rouge_n(prediction, batch.dec_output, 1)
                rouge_2 = rouge_n(prediction, batch.dec_output, 2)
                rouge_lcs = rouge_l_sentence_level(prediction, batch.dec_output)

                rouge_1_.append(rouge_1)
                rouge_2_.append(rouge_2)
                rouge_lcs_.append(rouge_lcs)

                cnt += len(samples)

                print('\rTesting - rouge_1: %0.3f, rouge_2: %0.3f, rouge_lcs: %0.3f, step: %d, line %d/%d'
                      % (float(np.mean(rouge_1_)), float(np.mean(rouge_2_)), float(np.mean(rouge_lcs_)),
                         global_step, cnt, n_data), end='')
                if escape:
                    break

        merged = sess.run(self.merged, feed_dict={self.rouge_1: float(np.mean(rouge_1_)),
                                                  self.rouge_2: float(np.mean(rouge_2_)),
                                                  self.rouge_lcs: float(np.mean(rouge_lcs_))})
        writer = tf.summary.FileWriter(os.path.join(self.logdir, 'test'),
                                       filename_suffix='-step-%d' % global_step)
        writer.add_summary(merged, global_step)

        print('\r', end='')
        print_write('Testing - rouge_1: %0.3f, rouge_2: %0.3f, rouge_lcs: %0.3f, step: %d, time: %s\n'
                    % (float(np.mean(rouge_1_)), float(np.mean(rouge_2_)), float(np.mean(rouge_lcs_)),  global_step,
                       time.strftime('%Hh %Mm %Ss', time.gmtime(time.time() - s))),
                    os.path.join(self.logdir, self.test_log), 'a')
        writer.close()
        return

    def run(self,
            sess: tf.Session,
            doc_string: str,
            tokenizer):
        tokens = tokenizer.morphs(doc_string)
        x = word_indexing(tokens, self.word2idx)
        prediction = sess.run(self.prediction,
                              feed_dict={self.encoder_input: [x], self.encoder_seq_len: [len(x)]})
        summ = indexed_word_revert(prediction[0, :, 0], self.idx2word, upto_eos=True)
        tagged = tokenizer.pos(''.join(summ))
        return inverse_tagging(tagged)

    def _embeddings(self, embedding_vectors):
        if embedding_vectors is not None:
            embed_init = tf.constant_initializer(embedding_vectors, verify_shape=True)
        else:
            embed_init = tf.random_normal_initializer()
        embeddings = tf.get_variable('embedding_vectors',
                                     shape=[self.vocabulary_size, self.embedding_size],
                                     dtype=tf.float32,
                                     initializer=embed_init,
                                     trainable=True)
        return embeddings

    def _encoder(self):
        fw_cells = rnn_cells(self.cell_type,
                             self.num_hidden,
                             self.num_layers,
                             self.keep_prob,
                             self.output_drop,
                             self.state_drop)
        bw_cells = rnn_cells(self.cell_type,
                             self.num_hidden,
                             self.num_layers,
                             self.keep_prob,
                             self.output_drop,
                             self.state_drop)

        outputs, state_fw, state_bw = \
            rnn.stack_bidirectional_dynamic_rnn(cells_fw=fw_cells,
                                                cells_bw=bw_cells,
                                                inputs=self.encoder_emb_inp,
                                                sequence_length=self.encoder_seq_len,
                                                dtype=tf.float32)

        state_c = tf.concat((state_fw[-1].c, state_bw[-1].c), 1)
        state_h = tf.concat((state_fw[-1].h, state_bw[-1].h), 1)
        return outputs, rnn.LSTMStateTuple(c=state_c, h=state_h)

    def _attention(self, d_a, r):
        self.W_s1 = tf.get_variable('W_s1',
                                    shape=[d_a, 2 * self.num_hidden],
                                    initializer=tf.contrib.layers.xavier_initializer())
        self.W_s2 = tf.get_variable('W_s2',
                                    shape=[r, d_a],
                                    initializer=tf.contrib.layers.xavier_initializer())

        a = tf.tanh(tf.map_fn(lambda x: tf.matmul(self.W_s1, tf.transpose(x)), self.encoder_output))
        a = tf.nn.softmax(tf.map_fn(lambda x: tf.matmul(self.W_s2, x), a))

        m = tf.matmul(a, self.encoder_output)

        a_t = tf.transpose(a, perm=[0, 2, 1])
        tile_eye = tf.tile(tf.eye(r), [tf.shape(self.encoder_input)[0], 1])
        tile_eye = tf.reshape(tile_eye, [-1, r, r])
        aa_t = tf.matmul(a, a_t) - tile_eye
        self.P = tf.square(tf.norm(aa_t, axis=[-2, -1], ord='fro'))
        return m, a

    def _decoder_trainer(self, scope):
        decoder_input = tf.concat([self.M, self.decoder_emb_inp], axis=2)
        helper = seq2seq.TrainingHelper(inputs=decoder_input,
                                        sequence_length=self.decoder_seq_len,
                                        time_major=False)
        decoder = seq2seq.BasicDecoder(self.decoder_cell, helper, self.encoder_state)
        outputs, _, _ = seq2seq.dynamic_decode(decoder, output_time_major=False, scope=scope)
        logits = self.projection_layer(outputs.rnn_output)
        pad = tf.zeros([self.batch_size,
                        DEC_MAX_LENGTH-tf.shape(logits)[1],
                        self.vocabulary_size])
        logits = tf.concat([logits, pad], axis=1)
        return logits

    # def _decoder_generator(self, scope):
    #     helper = MyHelper(embedding=self.embeddings,
    #                       m=self.M,
    #                       start_tokens=tf.fill([self.batch_size], tf.constant(SPECIAL_WORDS.index(GO))),
    #                       end_token=tf.constant(SPECIAL_WORDS.index(EOS)))
    #
    #     decoder = seq2seq.BasicDecoder(self.decoder_cell, helper, self.encoder_state)
    #     outputs, _, _ = seq2seq.dynamic_decode(decoder,
    #                                            maximum_iterations=DEC_MAX_LENGTH,
    #                                            output_time_major=False,
    #                                            scope=scope)
    #     logits = self.projection_layer(outputs.rnn_output)
    #     sample_id = tf.argmax(logits, axis=2)
    #     return sample_id

    def _decoder_generator(self, scope):
        tiled_encoder_state = seq2seq.tile_batch(self.encoder_state, multiplier=self.beam_width)
        decoder = MyBeamSearchDecoder(
            cell=self.decoder_cell,
            m=self.M,
            embedding=self.embeddings,
            start_tokens=tf.fill([self.batch_size], tf.constant(SPECIAL_WORDS.index(GO))),
            end_token=tf.constant(SPECIAL_WORDS.index(EOS)),
            initial_state=tiled_encoder_state,
            beam_width=self.beam_width,
            output_layer=self.projection_layer
        )
        outpus, _, _ = seq2seq.dynamic_decode(decoder,
                                              maximum_iterations=DEC_MAX_LENGTH,
                                              output_time_major=False,
                                              scope=scope)
        # print(outpus.predicted_ids)
        return outpus.predicted_ids

    def _get_loss(self, p_coef):
        crossent = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.logit, labels=self.decoder_target)
        weights = tf.sequence_mask(self.decoder_seq_len, DEC_MAX_LENGTH, dtype=tf.float32)
        loss = tf.reduce_sum(crossent * weights / tf.to_float(self.batch_size))
        return loss + (p_coef*self.P)

    def _get_optimizer(self):
        params = tf.trainable_variables()
        gradients = tf.gradients(self.loss, params)
        clipped_gradients, _ = tf.clip_by_global_norm(gradients, 5.0)
        optimizer = tf.train.AdamOptimizer(self.lr)
        return optimizer.apply_gradients(zip(clipped_gradients, params), global_step=self.global_step)


class MyBeamSearchDecoder(seq2seq.BeamSearchDecoder):
    def __init__(self,
                 cell,
                 m,
                 embedding,
                 start_tokens,
                 end_token,
                 initial_state,
                 beam_width,
                 output_layer):
        super(MyBeamSearchDecoder, self).__init__(cell,
                                                  embedding,
                                                  start_tokens,
                                                  end_token,
                                                  initial_state,
                                                  beam_width,
                                                  output_layer)

        def infer_input(time, ids):
            embed = tf.nn.embedding_lookup(embedding, ids)
            m_row = tf.gather(m, time + 1, axis=1)
            m_row = tf.expand_dims(m_row, axis=1)
            m_row = tf.tile(m_row, [1, beam_width, 1])
            return tf.concat([m_row, embed], axis=2)

        self._embedding_fn = infer_input
        self._start_inputs = self._embedding_fn(-1, self._start_tokens)

    def step(self, time, inputs, state, name=None):
        """Perform a decoding step.

            Args:
              time: scalar `int32` tensor.
              inputs: A (structure of) input tensors.
              state: A (structure of) state tensors and TensorArrays.
              name: Name scope for any created operations.

            Returns:
              `(outputs, next_state, next_inputs, finished)`.
            """
        batch_size = self._batch_size
        beam_width = self._beam_width
        end_token = self._end_token
        length_penalty_weight = self._length_penalty_weight

        with ops.name_scope(name, "BeamSearchDecoderStep", (time, inputs, state)):
            cell_state = state.cell_state
            inputs = nest.map_structure(
                lambda inp: self._merge_batch_beams(inp, s=inp.shape[2:]), inputs)
            cell_state = nest.map_structure(self._maybe_merge_batch_beams, cell_state,
                                            self._cell.state_size)
            cell_outputs, next_cell_state = self._cell(inputs, cell_state)
            cell_outputs = nest.map_structure(
                lambda out: self._split_batch_beams(out, out.shape[1:]), cell_outputs)
            next_cell_state = nest.map_structure(
                self._maybe_split_batch_beams, next_cell_state, self._cell.state_size)

            if self._output_layer is not None:
                cell_outputs = self._output_layer(cell_outputs)

            beam_search_output, beam_search_state = _beam_search_step(
                time=time,
                logits=cell_outputs,
                next_cell_state=next_cell_state,
                beam_state=state,
                batch_size=batch_size,
                beam_width=beam_width,
                end_token=end_token,
                length_penalty_weight=length_penalty_weight)

            finished = beam_search_state.finished
            sample_ids = beam_search_output.predicted_ids
            next_inputs = control_flow_ops.cond(
                math_ops.reduce_all(finished), lambda: self._start_inputs,
                lambda: self._embedding_fn(time, sample_ids))

        return beam_search_output, beam_search_state, next_inputs, finished


# class MyHelper(seq2seq.GreedyEmbeddingHelper):
#     def __init__(self, embedding, m, start_tokens, end_token):
#         super(MyHelper, self).__init__(embedding, start_tokens, end_token)
#
#         def infer_input(time, ids):
#             m_row = tf.gather(m, time, axis=1)
#             embed = tf.nn.embedding_lookup(embedding, ids)
#             return tf.concat([m_row, embed], axis=1)
#
#         self._embedding_fn = infer_input
#         self._start_inputs = self._embedding_fn(0, self._start_tokens)
#
#     def next_inputs(self, time, outputs, state, sample_ids, name=None):
#         finished = math_ops.equal(sample_ids, self._end_token)
#         all_finished = math_ops.reduce_all(finished)
#         next_inputs = control_flow_ops.cond(
#             all_finished,
#             # If we're finished, the next_inputs value doesn't matter
#             lambda: self._start_inputs,
#             lambda: self._embedding_fn(time, sample_ids))
#         return finished, next_inputs, state


if __name__ == "__main__":
    model = '../train_model/test'
    embed_model = '../word2vec/models/Kkma_d300_w08_mc2/word2vec.embed'
    word2idx, vectors, embed_size = load_word2vec(embed_model)
    summmarizer = Summarizer(logdir=model,
                             cell_type='LSTM',
                             word2idx=word2idx,
                             embedding_size=300,
                             num_hidden=256,
                             num_layers=2,
                             beam_width=10,
                             embedding_vectors=vectors,
                             forward_only=True,
                             batch_size=32,
                             keep_prob=1.0)
