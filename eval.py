from models.doc_embedding import *

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

model = 'train_model/doc_embedding_P1e-2_lstm2_256_kkma300_8_2'
embed_model = 'word2vec/models/Kkma_d300_w08_mc2/word2vec.embed'

word2idx, vectors, embed_size = load_word2vec(embed_model)
test_data = pickle_load('data/EncDecData/Kkma/test.p')

summmarizer = Summarizer(logdir=model,
                         cell_type='LSTM',
                         word2idx=word2idx,
                         embedding_size=embed_size,
                         num_hidden=256,
                         num_layers=2,
                         embedding_vectors=vectors,
                         forward_only=True,
                         batch_size=32,
                         keep_prob=1.0,
                         beam_width=10,
                         p_coef=1e-2)
sess = tf.Session(config=get_tf_config())
summmarizer.eval(sess, test_data, ckpt='weights-110000')
summmarizer.eval(sess, test_data, ckpt='weights-120000')
summmarizer.eval(sess, test_data, ckpt='weights-130000')
summmarizer.eval(sess, test_data, ckpt='weights-140000')
summmarizer.eval(sess, test_data, ckpt='weights-150000')

sess.close()
