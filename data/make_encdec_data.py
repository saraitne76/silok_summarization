from utils import *


def data_write(word2vec_model, target):
    word2idx, vectors, size = load_word2vec(word2vec_model)

    # ========================train data==========================================

    source = os.path.join('tokenized', target)
    destination = os.path.join('EncDecData', target)
    os.makedirs(destination, exist_ok=True)

    f = open(os.path.join('EncDecData', target, 'info.txt'), 'w')
    f.write('train==========================================================\n')
    train_data = list()
    cnt = np.zeros((len(ENC_BUCKET)+1), np.uint32)
    train_tokenized = read_data(os.path.join(source, 'train'))
    n_data = len(train_tokenized)
    for i, src in enumerate(train_tokenized):
        if TITLE_MAX_LENGTH >= len(src.title_token) >= 1 and DOC_BUCKET[-1] >= len(src.doc_token) >= 1:
            data = EncDecData(src, word2idx)
            train_data.append(data)
            bucket_size = get_bucket_size(src.doc_token)
            cnt[ENC_BUCKET.index(bucket_size)] += 1
        else:
            cnt[-1] += 1
        print('\r%s train data make %d/%d' % (target, i, n_data), end='')
    print()

    del train_tokenized
    pickle_store(train_data, os.path.join(destination, 'train.p'))
    del train_data

    # =========================test data==========================================

    for i in range(len(ENC_BUCKET)):
        f.write('Bucket %d: %d\n' % (ENC_BUCKET[i], cnt[i]))
    f.write('Exceed bucket %d\n' % cnt[-1])
    f.write('Written Data: %d\n' % cnt[:-1].sum())
    f.write('===============================================================\n')

    f.write('test===========================================================\n')
    test_data = list()
    cnt = np.zeros((len(ENC_BUCKET) + 1), np.uint32)
    test_tokenized = read_data(os.path.join(source, 'test'))
    n_data = len(test_tokenized)
    for i, src in enumerate(test_tokenized):
        if len(src.title_token) <= TITLE_MAX_LENGTH and len(src.doc_token) <= DOC_BUCKET[-1]:
            data = EncDecData(src, word2idx)
            test_data.append(data)
            bucket_size = get_bucket_size(src.doc_token)
            cnt[ENC_BUCKET.index(bucket_size)] += 1
        else:
            cnt[-1] += 1
        print('\r%s test data make %d/%d' % (target, i, n_data), end='')
    print()

    del test_tokenized
    pickle_store(test_data, os.path.join(destination, 'test.p'))
    del test_data

    for i in range(len(ENC_BUCKET)):
        f.write('Bucket %d: %d\n' % (ENC_BUCKET[i], cnt[i]))
    f.write('Exceed bucket %d\n' % cnt[-1])
    f.write('Written Data: %d\n' % cnt[:-1].sum())
    f.write('===============================================================\n')


def main():
    word2vec_models = ['../word2vec/models/Kkma_d300_w08_mc2/word2vec.embed',
                       '../word2vec/models/Mecab_d300_w08_mc2/word2vec.embed',
                       '../word2vec/models/Okt_d300_w08_mc2/word2vec.embed']
    target = ['Kkma', 'Mecab', 'Okt']

    for m, t in zip(word2vec_models, target):
        data_write(m, t)


if __name__ == '__main__':
    main()
