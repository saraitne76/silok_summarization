from utils import *
import re
from konlpy.tag import Kkma, Komoran, Hannanum, Okt, Mecab
from typing import Optional, Tuple
from multiprocessing import Process, Queue


PATTERN = '\(\w+\)|\d+\)|〔\w+〕'     # remove hanja & number of juseok


def get_txt_paths(basic_path, kings):
    txts = []
    for king in kings:
        king_path = os.path.join(basic_path, king)
        books = os.listdir(king_path)
        for book in books:
            book_path = os.path.join(king_path, book)
            texts = os.listdir(book_path)
            for text in texts:
                text_path = os.path.join(book_path, text)
                txts.append(text_path)
    return txts


def file2line(file_path: str,
              re_pattern) -> Tuple[str, str]:
    kr = get_kr(file_path).split('\n')
    kr = remove_redundant_line(kr, check=1)
    kr = ' '.join(kr)
    kr = re_pattern.sub('', kr)

    title = get_title(file_path).rstrip()
    title = re_pattern.sub('', title)

    return title, kr


def data_write(destination: str,
               pickle_name: str,
               files: List[str],
               q: Queue,
               tagger_class):
    pattern = re.compile(PATTERN)
    tagger = tagger_class()
    data_list = list()
    while q.qsize() > 0 or not q.empty():
        txt = files[q.get()]
        f = '/'.join(txt.split('/')[-3:])
        title, kr = file2line(txt, pattern)
        try:
            title_token = tagger.morphs(title)
            kr_token = tagger.morphs(kr)
            tokenized = Tokenized(file_name=f,
                                  title_raw=title,
                                  title_token=title_token,
                                  doc_raw=kr,
                                  doc_token=kr_token)
            data_list.append(tokenized)
        except ValueError:
            pass
        print('\rRemain Data %d' % (q.qsize()), end='')
        if q.qsize() < 0 or q.empty():
            break
    os.makedirs(destination, exist_ok=True)
    pickle_store(data_list, os.path.join(destination, pickle_name))
    print('\n%s store' % pickle_name)


def run_multiprocess(n_process, destination, kings, tagger_class):
    txt_files = get_txt_paths(SILOK_PATH, kings)
    n_files = len(txt_files)
    print('The number of text_files : %d' % n_files)

    q = Queue()
    for i in range(n_files):
        q.put(i)

    processes = []
    for i in range(n_process):
        p = Process(target=data_write,
                    args=(destination, 'data_%02d.p' % i, txt_files, q, tagger_class))
        p.start()
        processes.append(p)

    for p in processes:
        p.join()


def main():
    number_of_processes = 8
    make_seq = [(kings_train, Kkma, 'tokenized/Kkma/train'),
                (kings_test, Kkma, 'tokenized/Kkma/test')]

    for kings, tagger, target_folder in make_seq:
        run_multiprocess(number_of_processes, target_folder, kings, tagger)


if __name__ == '__main__':
    main()
