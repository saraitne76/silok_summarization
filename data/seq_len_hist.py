from utils import *
import matplotlib.pyplot as plt
import numpy as np


def main():
    n_bins = 20
    target = 'tokenized/Kkma'

    train_data = read_data(os.path.join(target, 'train'))
    test_data = read_data(os.path.join(target, 'test'))

    train_kr_len = list()
    train_title_len = list()
    for d in train_data:
        train_kr_len.append(len(d.kr_token))
        train_title_len.append(len(d.title_token))

    test_kr_len = list()
    test_title_len = list()
    for d in test_data:
        test_kr_len.append(len(d.kr_token))
        test_title_len.append(len(d.title_token))

    titles = ['train_kr', 'train_title', 'test_kr', 'test_title']
    hist_list = [np.clip(train_kr_len, 0, 500),
                 np.clip(train_title_len, 0, 50),
                 np.clip(test_kr_len, 0, 500),
                 np.clip(test_title_len, 0, 50)]
    fig = plt.figure()
    fig.suptitle(target)
    for i in range(4):
        ax = fig.add_subplot(2, 2, i+1)
        ax.set_title(titles[i])
        ax.hist(hist_list[i], n_bins)

    plt.show()


if __name__ == '__main__':
    main()
